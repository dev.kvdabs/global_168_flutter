class DateTimeUtil {
    public timestamp(): number {
        return new Date().getTime();
    }
}