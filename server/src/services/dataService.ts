// mongoClient.ts
import { MongoClient, Db, Collection } from 'mongodb';

class MongoDBClient {
  private static instance: MongoDBClient;
  private readonly client: MongoClient;
  public readonly database: Db;

  private constructor() {
    const url = 'mongodb://localhost:27017'; // Replace with your MongoDB server URL
    const dbName = 'global168'; // Replace with your database name

    this.client = new MongoClient(url, { });
    this.database = this.client.db(dbName);    
  }

  public static getInstance(): MongoDBClient {
    if (!MongoDBClient.instance) {
      MongoDBClient.instance = new MongoDBClient();
    }
    return MongoDBClient.instance;
  }

  public getDatabase(): Db {
    return this.database;
  }

  public async connect(): Promise<void> {
    try {
      await this.client.connect();
      console.log('Connected to MongoDB');
    } catch (err) {
      console.error('Error connecting to MongoDB:', err);
      throw err;
    }
  }

  public close(): void {
    this.client.close();
  }

  // collection

  public User(): Collection {
    return this.database.collection("User");
  }

  public Customer(): Collection {
    return this.database.collection("Customer");
  }

  public Truck(): Collection {
    return this.database.collection("Truck");
  }

  public Delivery(): Collection {
    return this.database.collection("Delivery");
  }
}

export default MongoDBClient;
