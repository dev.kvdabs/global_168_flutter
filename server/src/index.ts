import express from 'express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import DataService from './services/dataService';

const SECRET_KEY = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';
const app = express();
var cors = require('cors');

app.use(cors({
    origin: "*"
  }));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connect to MongoDB
DataService.getInstance().connect().then(() => console.log("connected to database")).catch((e) => console.error(e));;

// Authentication Middleware
const authenticateToken = (req: any, res: any, next: any) => {
    const token = req.header('Authorization');
    console.log(token);
    if (!token) {
        return res.status(401).send('Access denied');
    } 

    jwt.verify(token.split(' ')[1], SECRET_KEY, (err: any, user: any) => {
        if (err) { 
            console.error(err)
            return res.status(403).send('Invalid token');
        }
        req.user = user;
        next();
    });
};

// Routes
app.use('/user', require('./api/user_api'));
app.use('/customer', authenticateToken, require('./api/customer_api'));
app.use('/delivery', authenticateToken, require('./api/delivery_api'));


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});