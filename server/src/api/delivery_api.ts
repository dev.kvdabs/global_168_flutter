import express from "express";
import DataService from '../services/dataService';
import { ObjectId } from "mongodb";

const router = express.Router();
const Delivery = DataService.getInstance().Delivery();
const Customer = DataService.getInstance().Customer();

router.post("/list", (req, res) => {
    const filters = req.body;
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;

    let delivery = await Delivery.findOne({ _id: new ObjectId(id) });
    res.json(delivery);
});

router.post("/add", async (req, res) => {
    let data = req.body;

    const customerId = data['customerId'];
    const delivererId = data['delivererId'];
    const timestamp = new Date().getTime();

    data['_created_at'] = timestamp;
    data['_updated_at'] = timestamp;

    if (customerId && delivererId) {

        const customer = await Customer.findOne({ _id: new ObjectId(customerId) });
        if (customer) {
            let _deliverers: string[] = customer.deliverers;

            // add if deliverer not yet in Customer's list
            if (!_deliverers.find(delivererId)) {

                _deliverers.push(delivererId);

                Customer.updateOne({
                    _id: new ObjectId(data['customerId'])
                }, {
                    $set: {
                        deliverers: _deliverers
                    }
                })
            }            
        }
    }    

    let result = await Delivery.insertOne(data);
    res.json(result.insertedId);
});

router.post("/update", (req: any, res) => {
    let updates = req.body;
    const id = updates['_id'];

    delete updates['_id'];

    const timestamp = new Date().getTime();

    updates['_updated_at'] = timestamp;
    updates['_updated_by'] = req.user.username;

    Delivery.updateOne(
        { 
            _id: new ObjectId(id)
        },
        {
            $set: updates
        }
    );

    res.json(true);
});

module.exports = router;