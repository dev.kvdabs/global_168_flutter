import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import DataService from '../services/dataService';

const router = express.Router();
const SECRET_KEY = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'; // TODO: transfer to environment variable

const db = DataService.getInstance();

router.post('/register', async (req, res) => {
    const { username, password } = req.body;
    
    const hashedPassword = await bcrypt.hash(password, 10);
  
    const user = await db.User().findOne({ username });
    if (user != null) return res.status(401).send('Username already exist');

    await db.User().insertOne({ username, password: hashedPassword });

    res.status(201).send('User registered successfully');
});

router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    const user = await db.User().findOne({username});

    if (!user) {
        return res.status(401).send('User not found');
    }

    const isPasswordValid = await bcrypt.compare(password, user.password!);

    if (!isPasswordValid) return res.status(401).send('Invalid password');

    // Generate a JWT token for the user
    const token = jwt.sign({ username: user.username }, SECRET_KEY);
    
    res.json({ _id: user._id, token });
});

module.exports = router;