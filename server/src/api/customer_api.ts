import express from "express";
import DataService from '../services/dataService';
import { ObjectId } from "mongodb";


const router = express.Router();
const Customer = DataService.getInstance().Customer();

router.post("/list/:searchKey", async (req, res) => {
    const searchKey = req.params.searchKey;

    let data = await Customer.aggregate([
        {
            $sort: { name: 1 }
        }
    ]).toArray();

    res.json(data);
});

router.post("/defaultByDeliverer/:delivererId", async (req, res) => {
    const delivererId = (req.params.delivererId || '');

    if (delivererId.length == 0) return res.json([]);

    let data = await Customer.aggregate([
        {
            $match: {
                deliverers: { $in: delivererId }
            }
        },
        {
            $sort: { name: 1 }
        }
    ]).toArray();

    res.json(data);
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;

    let customer = await Customer.findOne({ _id: new ObjectId(id) });

    res.json(customer);
});

router.post("/add", async (req, res) => {
    let data = req.body;

    const timestamp = new Date().getTime();

    data['_created_at'] = timestamp
    data['_updated_at'] = timestamp

    let result = await Customer.insertOne(data);
    res.json(result.insertedId);
});

router.post("/update", (req: any, res) => {
    let updates = req.body;
    const id = updates['_id'];

    delete updates['_id'];

    const timestamp = new Date().getTime();

    updates['_updated_at'] = timestamp;
    updates['_updated_by'] = req.user.username;

    try {
        Customer.updateOne(
            { 
                _id: new ObjectId(id)
            },
            {
                $set: updates
            }
        );
    
        res.json(true);
    } catch (error) {
        console.error(error);
        res.json(false);
    }
});

module.exports = router;