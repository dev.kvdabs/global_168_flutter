import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:flutter_config/flutter_config.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:global168/data/repo/delivery_repository.dart';
import 'package:global168/data/repo/user_repository.dart';

import 'package:global168/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await FlutterConfig.loadEnvVariables();

  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepository>(
          create: (context) => UserRepository(),
        ),
        RepositoryProvider<DeliveryRepository>(
          create: (context) => DeliveryRepository(),
        )
      ],
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: Routes.login,
        getPages: Routes.pages,
      ),
    );
  }
}
