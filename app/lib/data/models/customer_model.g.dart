
part 'customer_model.dart';

Customer _$CustomerFromJson(Map<String, dynamic> json) {
    return Customer(
      id: json['_id'] ?? '',
      name: json['name'] ?? '',
    );
}