part of 'customer_model.g.dart';

class Customer {
    final String id;
    final String name;
  
    Customer({required this.id, required this.name});
  
    factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json); 
  }