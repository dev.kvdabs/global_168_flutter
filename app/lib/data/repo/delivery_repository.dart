import 'dart:convert';

///import 'package:flutter_config/flutter_config.dart';
import 'package:http/http.dart' as http;

class DeliveryRepository {
  String baseApi = "http://localhost:3000/delivery"; //FlutterConfig.get("BASE_API") + "/delivery";

  Future add(Map<String, dynamic> data) async {
    final response = await http.post(Uri.parse('$baseApi/add'), body: data);

    return jsonDecode(response.body);
  }

  Future update(Map<String, dynamic> data) async {
    final response = await http.post(Uri.parse('$baseApi/update'), body: data);

    return jsonDecode(response.body);
  }

  Future list(Map<String, dynamic> data) async {
    final response = await http.post(Uri.parse('$baseApi/list'), body: data);
    
    return jsonDecode(response.body);
  }
}