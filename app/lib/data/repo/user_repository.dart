import 'dart:convert';

//import 'package:flutter_config/flutter_config.dart';
import 'package:http/http.dart' as http;

class UserRepository {
  String baseApi = "http://localhost:3000/user"; //FlutterConfig.get("BASE_API") + "/user";

  Future login(String username, String password) async { 

    var result = await http.post(Uri.parse('$baseApi/login'), body: {
      'username': username,
      'password': password
    });

    if (result.statusCode == 200) {
      return {
        'success': true,
        'data': jsonDecode(result.body)
      };
    }
    
    return {
      'success': false,
      'errorMessage': result.body
    };
  }
}