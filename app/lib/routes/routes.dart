import 'package:get/get.dart';
import 'package:global168/pages/delivery/screens/new_delivery_screen.dart/new_delivery_screen.dart';
import 'package:global168/pages/login/login.dart';
import 'package:global168/pages/nav/nav.dart';

class Routes {
  static List<GetPage> pages = [
    GetPage(name: login, page: () => const LoginScreen()),
    GetPage(name: nav, page: () => const NavScreen()),
    GetPage(name: newDelivery, page: () => const NewDeliveryScreen())
  ];

  static String login = "/login";
  static String nav = "/nav";
  static String newDelivery = "/new-delivery";  
}