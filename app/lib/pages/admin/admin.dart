import 'package:flutter/material.dart';

class AdminScreen extends StatefulWidget {
  const AdminScreen({super.key});

  @override
  State<AdminScreen> createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3, // The number of tabs
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Administration"),
            // actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_horiz_sharp))],
            bottom: const TabBar(
              tabs: [
                Tab(text: 'Users'),
                Tab(text: 'Customers'),
                Tab(text: 'Trucks'),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(onPressed: () {}, child: Icon(Icons.add),),
          body: TabBarView(
            children: [
              // Content for Tab 1
              ListView(
                children: [ListTile(title: Text("Jouel Ong"), subtitle: Text("Deliverer"),)],
              ),

              // Content for Tab 2
              ListView(
                      children: [ListTile(title: Text("Kusina Uno"), subtitle: Text("GND Bldg. Mabolo, Cebu City"),)],
                    )  ,
              
              // Content for Tab 3
              ListView(
                children: [
                  ListTile(title: Text("GAJ 8362"),),
                  ListTile(title: Text("AXA 8264"),),
                  ListTile(title: Text("BAU 9982"),),
                  ListTile(title: Text("OTW 1290"),),
                  ListTile(title: Text("FYI 4453"),)
                ],
              ),
            ],
          ),
        ),
      );
  }
}