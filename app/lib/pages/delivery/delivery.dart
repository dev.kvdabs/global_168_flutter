import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:global168/data/repo/delivery_repository.dart';
import 'package:global168/pages/delivery/bloc/delivery_bloc.dart';
import 'package:global168/pages/delivery/screens/new_delivery_screen.dart/new_delivery_screen.dart';
import 'package:global168/routes/routes.dart';

class DeliveryScreen extends StatefulWidget {
  const DeliveryScreen({super.key});

  @override
  State<DeliveryScreen> createState() => _DeliveryScreenState();
}

class _DeliveryScreenState extends State<DeliveryScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DeliveryBloc(RepositoryProvider.of<DeliveryRepository>(context)),
      child: BlocListener<DeliveryBloc, DeliveryState>(
        listener: (context, state) {
          // TODO: implement listener
        },
        child: BlocBuilder<DeliveryBloc, DeliveryState>(
          builder: (context, state) {
            return Scaffold(
                floatingActionButton: FloatingActionButton(
                  onPressed: () {
                    Get.toNamed(Routes.newDelivery);
                  },
                  child: const Icon(Icons.add),
                ),
                floatingActionButtonAnimator:
                    FloatingActionButtonAnimator.scaling,
                body: const Center(
                  child: Text("Delivery"),
                ));
          },
        ),
      ),
    );
  }
}
