part of 'delivery_bloc.dart';

@immutable
sealed class DeliveryState extends Equatable {
  @override
  List<Object?> get props => [];
}

class DeliveryInitialState extends DeliveryState {}

class DeliveryFetchingState extends DeliveryState {}

class DeliveryFetchingFailedState extends DeliveryState {}

class DeliveryFetchingSuccessState extends DeliveryState {}
