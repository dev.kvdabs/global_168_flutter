import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:global168/data/repo/delivery_repository.dart';
import 'package:meta/meta.dart';

part 'delivery_event.dart';
part 'delivery_state.dart';

class DeliveryBloc extends Bloc<DeliveryEvent, DeliveryState> {
  final DeliveryRepository _repository;
  
  DeliveryBloc(this._repository) : super(DeliveryInitialState()) {
    
    on<AddDelivery>((event, emit) {

    });
  }
}
