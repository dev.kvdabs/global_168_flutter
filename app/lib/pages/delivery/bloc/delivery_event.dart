part of 'delivery_bloc.dart';

@immutable
sealed class DeliveryEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class AddDelivery extends DeliveryEvent {}