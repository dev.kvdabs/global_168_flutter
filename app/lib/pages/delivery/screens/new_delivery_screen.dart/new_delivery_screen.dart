import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:global168/data/models/customer_model.g.dart';
import 'package:global168/pages/delivery/bloc/delivery_bloc.dart';

class NewDeliveryScreen extends StatefulWidget {
  const NewDeliveryScreen({super.key});

  @override
  State<NewDeliveryScreen> createState() => _NewDeliveryScreenState();
}

class _NewDeliveryScreenState extends State<NewDeliveryScreen> {
  int tag = 1;
  final TextEditingController _typeAheadController = TextEditingController();
  String _selectedTruck = '';
  Customer? _selectedCustomer;
  @override
  Widget build(BuildContext context) {
    //final deliveryBloc = BlocProvider.of<DeliveryBloc>(context);

    
    return Scaffold(
      backgroundColor: ThemeData().scaffoldBackgroundColor,
      appBar: AppBar(elevation: 0,  title: const Text("New Delivery", )),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                const SizedBox(height: 20,),

                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6))
                  ),
                  child: InkWell(
                    onTap: () {
                      var customers = <Customer>[];

                      customers.add(Customer(id: "123", name: "Kusina Uno"));
                      customers.add(Customer(id: "456", name: "Kusina Dos"));
                      customers.add(Customer(id: "789", name: "Kusina Tres"));
                      customers.add(Customer(id: "111", name: "Kusina Kwatro"));
                      customers.add(Customer(id: "222", name: "Kusina Singko"));
                      customers.add(Customer(id: "333", name: "Kusina Sais"));
                      customers.add(Customer(id: "444", name: "Kusina Syete"));


                      showSearch(context: context, delegate: CustomSearchDelegate(customers)).then((value) {
                        if (value != null) {
                          setState(() {
                            _selectedCustomer = value;
                          });
                        }
                      });
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const Text("Customer", style: TextStyle(fontWeight: FontWeight.w700)),
                      const SizedBox(height: 10),
                      if (_selectedCustomer != null) Text(_selectedCustomer!.name)
                    ]),
                  ),
                ),

                const SizedBox(height: 20,),

                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(8),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6))
                  ),
                  child: InkWell(
                    onTap: () {
                      final trucks = ["AXA 1234", "GAJ 8362", "ABC 4567", "QWE 0987", "BHT 4518", "AXA 1234", "GAJ 8362", "ABC 4567", "QWE 0987", "BHT 4518"];

                      showModalBottomSheet(context: context, builder: ((context) {
                        return Scaffold(
                          appBar: AppBar(title: const Text("Select Truck")),
                          body: ListView.builder(
                            itemCount: trucks.length,
                            itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(trucks[index]),
                              onTap: () {
                                setState(() {
                                  _selectedTruck = trucks[index];
                                });
                                Navigator.pop(context);
                              },
                            );
                          }),
                        );
                      }));
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      const Text("Truck", style: TextStyle(fontWeight: FontWeight.w700), textAlign: TextAlign.left),
                      const SizedBox(height: 10),
                      Text(_selectedTruck)
                    // TypeAheadFormField(
                    //   enabled: false,
                    //     textFieldConfiguration: TextFieldConfiguration(
                    //       controller: this._typeAheadController,
                    //       //decoration: InputDecoration(labelText: 'City')
                    //     ),
                    //     suggestionsCallback: (pattern) =>
                    //         ["ABC 1234", "GAJ 8362", "AXA 0987"],
                    //     itemBuilder: (context, suggestion) => ListTile(
                    //       title: Text(suggestion),
                    //     ),
                    //     transitionBuilder: (context, suggestionsBox, controller) =>
                    //         suggestionsBox,
                    //     onSuggestionSelected: (suggestion) {
                    //       this._typeAheadController.text = suggestion;
                    //     },
                    //     validator: (value) =>
                    //         value!.isEmpty ? 'Please select a city' : null,
                    //     onSaved: (value) => this._selectedCity = value ?? '',
                    //   )
                    ]),
                  ),
                ),

                const SizedBox(height: 20,),

                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6))
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    const Text("Remarks", style: TextStyle(fontWeight: FontWeight.w700),),
                    const SizedBox(height: 10),
                TextFormField(minLines: 3, maxLines: 5,)
                  ]),
                ),

                // SizedBox(height: 20,),
                // Container(alignment: Alignment.center, child: ElevatedButton(child: Text("Save"), onPressed: () {},),)
              ],
            ),
            Padding(padding: const EdgeInsets.all(20), child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(child: const Text("Save"), onPressed: () {}, style: ButtonStyle(minimumSize: MaterialStateProperty.all(const Size(150, 50)))),
                TextButton(onPressed: () {}, child: const Text("Cancel", style: TextStyle(color: Colors.black45),), style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey.shade100), minimumSize: MaterialStateProperty.all(const Size(150, 50)))),  
              ],
            ),)
          ],
        ),
      ),
      //floatingActionButton: ElevatedButton(child: Text("Save"), onPressed: () {},),
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class CustomSearchDelegate extends SearchDelegate<Customer?> {
  final List<Customer> data; // Replace with your search data

  CustomSearchDelegate(this.data);

  @override
  List<Widget> buildActions(BuildContext context) {
    // Actions for the search bar (e.g., clear query button)
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = ''; // Clear the query
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Leading icon (e.g., back button)
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null); // Close the search and return null
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Build the search results based on the query
    final results = data.where((item) => item.name.contains(query)).toList();

    return ListView.builder(
      itemCount: results.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(results[index].name),
          // Add functionality when a result is selected
          onTap: () {
            close(context, results[index]);
          },
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Suggestions shown as the user types
    final suggestionList = data.where((item) => item.name.startsWith(query)).toList();

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(suggestionList[index].name),
          onTap: () {
            close(context, suggestionList[index]);
            // query = suggestionList[index].name; // Fill the query with the suggestion
            // showResults(context); // Show the results for this suggestion
          },
        );
      },
    );
  }
}
