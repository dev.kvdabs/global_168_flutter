import 'package:flutter/material.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.fromLTRB(8.0, 50, 8.0, 20.0),
      child: Column(
        children: [
          const CircleAvatar(
            minRadius: 100,
            backgroundColor: Colors.grey
          ),
          const SizedBox(height: 20,),
          const Text("Jouel Ong", style: TextStyle(fontSize: 22),),
          const SizedBox(height: 5,),
          const Text("Deliverer", style: TextStyle(fontSize: 16),),
          const SizedBox(height: 100),
          TextButton(onPressed: () {}, child: const Text("Change Password", style: TextStyle(fontSize: 20, color: Colors.blue))),
          const SizedBox(height: 20),
          TextButton(onPressed: () {}, child: const Text("Logout", style: TextStyle(fontSize: 20, color: Colors.redAccent)))
        ],
      ),
    );
    
  }
}