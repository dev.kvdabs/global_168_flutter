import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:global168/data/repo/user_repository.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _repository;
  
  LoginBloc(this._repository) : super(LoginInitialState()) {
    
    on<LoginButtonPressed>((event, emit) async {

      emit(AuthenticatingState());

      final response = await _repository.login(event.username, event.password);

      if (response['success']) {
        // TODO: save access token to cache
        return emit(LoginSuccessState(username: event.username));
      }

      emit(LoginFailedState(error: response['errorMessage']));

    });
  }

  

  //Stream<LoginState> mapEventToState(LoginEvent event) async* {
    // if (event is LoginButtonPressed) {
    //   yield LoginLoading();
    //   // Simulate a login process; replace this with your authentication logic.
    //   await Future.delayed(const Duration(seconds: 2));
    //   if (event.username == 'user' && event.password == 'password') {
    //     yield LoginSuccess();
    //   } else {
    //     yield LoginFailure(error: 'Invalid credentials');
    //   }
    // } else if (event is LogoutButtonPressed) {
    //   // Handle logout logic here.
    // }

    
  //}
}
