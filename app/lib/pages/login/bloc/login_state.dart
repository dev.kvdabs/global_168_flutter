part of 'login_bloc.dart';

sealed class LoginState extends Equatable {
  const LoginState();
  
  @override
  List<Object> get props => [];
}

class LoginInitialState extends LoginState {}

class AuthenticatingState extends LoginState {}

class LoginSuccessState extends LoginState {
  final String username;
  const LoginSuccessState({required this.username});
  @override
  List<Object> get props => [username];
}

class LoginFailedState extends LoginState {
  final String error;

  const LoginFailedState({required this.error});

  @override
  List<Object> get props => [error];
}
