import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:global168/data/repo/user_repository.dart';
import 'package:global168/pages/login/bloc/login_bloc.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    //final LoginBloc loginBloc = BlocProvider.of<LoginBloc>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Login Screen')),
      body: RepositoryProvider(
        create: (context) => UserRepository(),
        child: BlocProvider(
          create: (context) =>
              LoginBloc(RepositoryProvider.of<UserRepository>(context)),
          child: BlocListener<LoginBloc, LoginState>(
            listener: (context, state) async {
              if (state is AuthenticatingState) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Logging in...')),
                );
              } else if (state is LoginSuccessState) {
                Get.offNamed("/nav");
              }

              // Display an error message.
              else if (state is LoginFailedState) {
                // ScaffoldMessenger.of(context).showSnackBar(
                //   SnackBar(
                //       backgroundColor: Colors.red, content: Text(state.error)),
                // );

                
              }
            },
            child: BlocBuilder<LoginBloc, LoginState>(
              builder: (context, state) {
                return Center(
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextField(
                          decoration: InputDecoration(labelText: 'Email'),
                          onChanged: (value) {
                            // Handle email input.
                          },
                        ),
                        SizedBox(height: 16),
                        TextField(
                          decoration: InputDecoration(labelText: 'Password'),
                          obscureText: true,
                          onChanged: (value) {
                            // Handle password input.
                          },
                        ),
                        SizedBox(height: 16),
                        ElevatedButton(
                          onPressed: () {
                            Get.offNamed("/nav");
                            // BlocProvider.of<LoginBloc>(context)
                            //     .add(LoginButtonPressed(
                            //   username:
                            //       'macienulada', // Replace with actual input.
                            //   password:
                            //       '09260926', // Replace with actual input.
                            // ));
                          },
                          child: Text('Login'),
                        ),
                        _failedLogin(state)
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _failedLogin(LoginState state) {
    if (state is LoginFailedState) return Text(state.error);

    return Container();
  }
}
