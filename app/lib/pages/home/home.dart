import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Jouel Ong", style: TextStyle(color: Colors.white, fontSize: 20),),
        actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.more_horiz_sharp))],
        
      ),
      body: Center(
            child: Text('Tab 1 Content'),
          )
    );
  }
}