import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:global168/pages/account/account.dart';
import 'package:global168/pages/admin/admin.dart';
import 'package:global168/pages/collection/collection.dart';
import 'package:global168/pages/delivery/delivery.dart';
import 'package:global168/pages/home/home.dart';
import 'package:global168/pages/nav/nav_controller.dart';

class NavScreen extends StatefulWidget {
  const NavScreen({super.key});

  @override
  State<NavScreen> createState() => _NavScreen();
}

class _NavScreen extends State<NavScreen> {
  final controller = Get.put(NavController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavController>(builder: (context) {
      return Scaffold(      
      body: IndexedStack(
        index: controller.tabIndex,
        children: const [
          AccountScreen(),
          DeliveryScreen(),
          CollectionScreen(),
          AdminScreen()
        ],
      ),
      //floatingActionButton: FloatingActionButton(onPressed: () {}, child: const Icon(Icons.add),),
      //floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      //floatingActionButtonLocation: FloatingActionButtonLocation.c,
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        //type: BottomNavigationBarType.shifting,
        currentIndex: controller.tabIndex,
        onTap: controller.changeTabIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.person_2_sharp),
            label: 'Account',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.delivery_dining),
            label: 'Deliveries',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.currency_exchange),
            label: 'Collections',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Admin',
          ),
        ],
      ),
    );
    });
  }
}

